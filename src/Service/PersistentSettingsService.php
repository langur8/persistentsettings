<?php

namespace dlouhy\PersistentSettingsBundle\Service;


use dlouhy\PersistentSettingsBundle\Entity\EntityAbstract;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Description of PersistentSettingsService
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */
class PersistentSettingsService
{

	/**
	 * @var Registry
	 */
	private $doctrine;
	
	
	/**
	 * @var string
	 */
	private $entityName;
	
	
	public function __construct(Registry $r, $e)
	{
		$this->entityName = $e;
		$this->doctrine = $r;
	}
	
	
	public function get()
	{		
		$repo = $this->doctrine->getRepository($this->entityName);
		$entity = $repo->findOneBy(array('active' => 1));		
		return $entity;
		
	}
	
	
	public function set(EntityAbstract $e)
	{
		$em = $this->doctrine->getManager();
		$em->persist($e);
		$em->flush();
	}
	
}
